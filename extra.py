class TwoWayDict(dict):
    def __len__(self):
        return dict.__len__(self) / 2

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        dict.__setitem__(self, value, key)


##### createDictionary #####
# [purpose]: This function creates a two way dictionary to allow for easy association between variable name and variable alias.
#
# [input]: varList - List of tuples with the network's random variables.
#
# [output]: Two way dictionary.
def createDictionary(varList):
    dictionary = TwoWayDict()

    for var in varList:
        if var[2] != None:
            dictionary[var[0]] = var[2]

    return dictionary

##### createFactor #####
# [purpose]: This function merges the information from varList and cptList.
#
# [input]: varList - List of tuples with the network's random variables.
#          cptList - List of tuples with the random variables' conditional probability tables
#
# [output]: Dictionary with the Bayesian Network info.
def createFactor(varList, cptList, dictNameAlias):
    factorDict = {}

    for cpt in cptList:
        if len(cpt[0]) < len(dictNameAlias[cpt[0]]): # Alias
            key = dictNameAlias[cpt[0]]
        else:
            key = cpt[0]
        dependencies=  []
        for var in varList:
            if var[0] == key:
                if var[3] != None:
                    for element in list(var[3]):
                        if len(element) < len(dictNameAlias[element]):
                            dependencies.append(dictNameAlias[element])
                        else:
                            dependencies.append(element)
                    dependencies.insert(0, key)
                else:
                    dependencies = [key]
                factorDict[key] = (dependencies, cpt[1])

    return factorDict


##### normalization #####
# [purpose]: This function normalizes the results in order to sum to 1.
#
# [input]: table - denormalized 'probabilities' of the query variable
#
# [output]: Query probabilities.
def normalization(table):

    sum = 0
    for item in table:
        sum+= item[1]

    for i in range(len(table)):
        table[i][1] = table[i][1]/sum

    return table


##### normalization #####
# [purpose]: This function prints the factor to the console.
#
# [input]: factor - tuple with the factor dependencies and 'probability' table
def printtable(factor):

    print('\nFactor variables:\n')
    print(factor[0])
    print('\n')

    for row in factor[1]:
        print(row)



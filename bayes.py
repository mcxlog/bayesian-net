# bayes.py
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC
#

import utils

def sumout(factor, var, verbose):
    

    index = factor[0].index(var)

    if verbose:
        print('\nSumming-out with respect to: {0}\n'.format(var))

    newfactortable = []

    l = len(factor[1])

    i = 0

    for row in factor[1]:
        
        valuesList1 = list(row[0])
        
        value1 = valuesList1[index]

        del valuesList1[index]

        for j in range(i, l):
            
            rowcomp = factor[1][j]
            
            valuesList2 = list(rowcomp[0])

            value2 = valuesList2[index]
            
            del valuesList2[index]

            if value1 != value2 and valuesList1 == valuesList2:
                sum = row[1] + rowcomp[1]
                newfactortable.append((valuesList1, sum))

        i+=1

    newfactordep = list(factor[0])
    del newfactordep[index]
    newfactor = (newfactordep, newfactortable)

    if verbose:
        print('\nResulting factor:\n')
        utils.printtable(newfactor)

    
    return newfactor
        
    
def productTwoFactors(factor_A, factor_B, varList, verbose):

    if verbose:
        print('\nFactor product between:\n')
    
        print('--------First Factor---------')
        utils.printtable(factor_A)
    
        print('\n--------Second Factor---------')
        utils.printtable(factor_B)


    test = []
    for element1 in factor_A[1]:
        for element2 in factor_B[1]:
            row = (element1[0] + element2[0], element1[1]*element2[1])
            test.append(row)


    common = []
    dependenciesOut = []
    for i in factor_A[0] + factor_B[0]:
        if i not in dependenciesOut:
            dependenciesOut.append(i)
        else:
            common.append(i)


    table = []
    indices = []
    newtable = []
    for item in common:
        i = 0
        
        if table == []:
            commonIndexA = factor_A[0].index(item)
            commonIndexB = factor_B[0].index(item)


            for tup in test:
                if tup[0][commonIndexA] == tup[0][len(factor_A[0])+commonIndexB]:
                    listOut = list(tup[0])
                    del listOut[commonIndexA]
                    table.append((listOut, tup[1]))
                i+=1
            
            depOut = factor_A[0]+factor_B[0]
            del depOut[commonIndexA]
            
            newtable = list(table)
        
        else:
            table = []
            table = list(newtable)
            newtable = []
            
            commonIndices = [l for l, x in enumerate(depOut) if x == item]
            for tup in table:
                values = tup[0]
                
                if values[commonIndices[0]] == values[commonIndices[1]]:
                    listOut = list(values)
                    del listOut[commonIndices[0]]
                    newtable.append((listOut, tup[1]))
                i+=1
            
            del depOut[commonIndices[0]]

    newcpt = (depOut, newtable)

    sortedNewcpt = tablesort(newcpt)

    newcpt2 = (newcpt[0], sortedNewcpt)
    
    if verbose:
        print('\nResulting product:\n')
        utils.printtable(newcpt2)


    return newcpt2

def tablesort(cpt):

    i = 0
    weightlist = []
    for tup in cpt[1]:
        weight = 0
        l = len(tup[0])
        for j in range(l):
            weight+=ord(tup[0][j])**(l-j)
        weightlist.append((weight,i))
        i+=1
    
    weightlist.sort(reverse=True)
    sortedcpt = []
    for item in weightlist:
        sortedcpt.append(cpt[1][item[1]])
    
    return sortedcpt




def productFactors(factors, varList, verbose):
    
    if len(factors) == 1: # Product is the input factor alone
        return factors[0]

    index = 0
    l = len(factors)
    for i in range(l-1): # l-1 products between factors
        l = len(factors)-1

        # Collect and unpack two factors
        factor_A = factors[0]

        factor_B = factors[l]


        while len(set(factor_A[0]) & set(factor_B[0])) == 0:
#            print('\nNo common dependencies. Choose another factor.\n')
            l-=1
            factor_B = factors[l]



        acc = tuple(productTwoFactors(factor_A,factor_B, varList, verbose))

        del factors[l]
        del factors[0]
        factors.append(acc)


    return acc



def sumProductEliminateVar(dictAlias, factors, var, varList, verbose):

    factors_in  = [] # List of factors that depend on var 

    keydel = []

    for key,factor in factors.items():
        
        varListing = factor[0] # List of variables in factor
        
        if (var in varListing) or (dictAlias[var] in varListing):
            keydel.append(key)
            factors_in.append(factor)

    for key in keydel:
        del factors[key]


    factor_product = productFactors(factors_in, varList, verbose)

    if len(factor_product[0]) > 1:
        factors_marginalized = sumout(factor_product, var, verbose)
    else:
        factors_marginalized = factor_product



    key = ''
    for var in factors_marginalized[0]:
        key += var

    if key in factors:
        factors_marginalized = productFactors([factors[key], factors_marginalized],varList, 0)



    factors[key] = factors_marginalized


def sumProductVariableElimination(dictAlias, factors, variables, varList, verbose):
    
    for var in variables:
        if verbose:
            print('\n-------------------------------------\n')
            print("\n Eliminate var {0}\n".format(var))
        sumProductEliminateVar(dictAlias, factors, var, varList, verbose)

    l = []
    for key,val in factors.items():
        l.append(val)

    return productFactors(l, varList, verbose)


def hiddenSet(varList, alias, evidences, query, verbose, ordering):
    
    keylist = []
    evlist = []
    keydep = []
    for varInfo in varList:
        keylist.append(varInfo[0])
        if varInfo[3] != None:
            keydep.append(len(varInfo[3]))
        else:
            keydep.append(0)

    dictionary = dict(zip(keylist, keydep))

    for evidence in evidences:
        evlist.append(evidence[0])

    keyset = set(keylist)
    queryset = {query}
    evset = set(evlist)
    
    hiddenset = {}
    hiddenset = keyset - queryset
    hiddenset-= evset

    if ordering:

        for varInfo in varList:
            if varInfo[3] != None:
                for dep in varInfo[3]:
                    if len(dep) < len(alias[dep]): # Alias
                        dictionary[alias[dep]]+=1
                    else:
                        dictionary[dep]+=1


        hiddenweight = []
        for item in hiddenset:
            hiddenweight.append((item, dictionary[item]))

        hiddenweight = sorted(hiddenweight, key=lambda hiddenweight: hiddenweight[1])

        hidden = []
        for var in hiddenweight:
            hidden.append(var[0])
    else:
        hidden = list(hiddenset)
    
    if verbose:
        print('\nHidden variables: {0}'.format(hidden))

    return hidden

def applyEvidences(factor, evidences, variable):

    variable_index = factor[0].index(variable)

    for evidence in evidences:
        var = evidence[0]
        val = evidence[1]
        evidence.append(factor[0].index(var))

    listOfVar=[]
    for row in factor[1]:
        
        isFound=True
        for evidence in evidences:
            if row[0][evidence[2]] != evidence[1]:
                isFound = False
                break
        
        if isFound == True:
            listOfVar.append( [row[0][variable_index] , float(row[1]) ] )

    return listOfVar






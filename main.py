# main.py - Main script for BN network inference algorithm
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC
#

import sys
import BNspec
import utils
import bayes
import time

# Uncomment this line to run the script directly
# without having to provide arguments:
#
sys.argv = ['main.py','./test.txt','JohnCalls', 'Burglary', 't']

# 
# To avoid showing all algorithm steps , set verbose to 0
#
verbose = 1

if(len(sys.argv)<3):
    print('Usage: main.py INPUT_BN QUERY [EVIDENCE0] [VAL0] [EVIDENCE1] [VAL1]...')
    sys.exit()

file = sys.argv[1]

varList, cptList = BNspec.readBNspecs(file)

if varList is None or cptList is None:
    sys.exit('An error ocurred while reading input file...')

dictNameAlias = utils.createDictionary(varList)

factorDict = utils.createFactor(varList, cptList, dictNameAlias)

# Collect Query from arguments
query = sys.argv[2]

# Check evidence list size
nEvidenceList = (len(sys.argv)-3)

# Evidence list must have an even number of elements (any number of pairs(E,V))
if nEvidenceList%2 != 0:
    print('ERROR: Missing a value in evidence list')
    print('Usage: main.py INPUT_BN QUERY EVIDENCE0 VAL0 EVIDENCE1 VAL1...')
    sys.exit()

evidences = []
k=0
if nEvidenceList > 0:
    while k <= int(nEvidenceList/2):
        variable = sys.argv[3+k]
        value    = sys.argv[3+k+1]
        evidences.append([variable,value])
        k+=2

print("\nQuery: {0}".format(query))
print("Evidence: {0}".format(evidences))

#Define Evidence
# Evidences must not be conflicting otherwise it is impossible to solve
# Evidences should be specified as a list of lists

for evidence in evidences:
    for item in varList:
        belongs = False
        if evidence[0] == item[0]:
            belongs = True
            break
    if belongs is False:
        print('Evidence {0} is not in the Network. Exiting...'.format(evidence))
        sys.exit()

if len(evidences) != 0:
    evstatement = ' given that ' + evidences[0][0] + ' is ' + evidences[0][1]
    for i in range(1, len(evidences)):
        evstatement+= ' and ' + evidences[i][0] + ' is ' + evidences[i][1]
else:
    evstatement = ''

statement = 'Probabilty of ' + query + evstatement

print('\n--------------\nProblem statement: {0}\n'.format(statement))

if verbose:
    print("Input Bayesian Network:\n")
    print(factorDict)

# Define ordering = True for min-neighbours heuristic
ordering = True

hidden = bayes.hiddenSet(varList, dictNameAlias, evidences, query, verbose, ordering)

start=time.time()
phi = bayes.sumProductVariableElimination(dictNameAlias,factorDict,hidden, varList, verbose)
end=time.time()

listOfVar = bayes.applyEvidences(phi, evidences, query)

prob = utils.normalization(listOfVar)

result = ([query], prob)

print('\nPosterior probability of query variable {0}:'.format(query))

utils.printtable(result)

print('\nTime(VE): %fs'%(end-start))


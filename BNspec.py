# BNspec.py - Provides functions to read the input file
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC
#

##### readBNspecs #####
# [purpose]: This function reads the BN specifications from input file.
# [input]: fileName - name of the input file.
# [output]: Either the problem data or None or error.

def readBNspecs(fileName):

    print('Info: Reading input file...')
    nline=1
    fi = open(fileName)

    vars = []
    cpts = []

    line = fi.readline()

    while True:
        
        if len(line) == 0:
            break
        
        # Ignore comments
        if line.startswith('#'):
            line = fi.readline()
            continue

        # Check for variable line 'VAR'
        if line.startswith('VAR'):
            varName = None
            varValues = None
            varAlias = None
            varParents = None
            varFlag = True
            cptFlag = False
            line = fi.readline()
            continue
        
        # Check for variable line 'CPT'
        if line.startswith('CPT'):
            var = None
            table = None
            varFlag = False
            cptFlag = True
            line = fi.readline()
            continue

        if varFlag:

            while not len(line) == 0 and not line.startswith('VAR') and not line.startswith('CPT'):
                
                info = []
                
                if line.startswith('#'):
                    line = fi.readline()
                    continue
                
                # New random variable
                if line.startswith('name'):
                    info = line.split()
                    varName = info[1]

                if line.startswith('values'):
                    info = line.split()
                    nValues = len(info)-1
                    varValues = tuple(info[1:])
                        
                if line.startswith('alias'):
                    info = line.split()
                    varAlias = info[1]

                if line.startswith('parents'):
                    info = line.split()
                    varParents = tuple(info[1:])

                line = fi.readline()

            if varName != None and varValues != None:
                vars.append((varName, varValues, varAlias, varParents))
            else:
                print('Missing mandatory keys for VAR! Exiting...')
                return None, None



        elif cptFlag:
            
            while not len(line) == 0 and not line.startswith('VAR') and not line.startswith('CPT'):

                info = []
                
                if line.startswith('#'):
                    line = fi.readline()
                    continue
                
                if line.startswith('var'):
                    info = line.split()
                    var = info[1]
                
                if line.startswith('table'):
                    info = line.split()
                    table = []

                    probIndex = [i for i,x in enumerate(info) if (x.isdigit() or x.replace('.','',1).isdigit())]
                    if len(probIndex) == 0:
                        line = fi.readline()
                        while not line.startswith('VAR') and not line.startswith('CPT') and not len(line) == 0:
                            info = line.split()
                            probIndex = [i for i,x in enumerate(info) if (x.isdigit() or x.replace('.','',1).isdigit())]
                            table.append((info[0:probIndex[0]], float(info[probIndex[0]])))
                            line = fi.readline()
                    else:
                        for i in probIndex:
                            table.append((info[i+1-probIndex[0]:i], float(info[i])))
    

                if not line.startswith('VAR') and not line.startswith('CPT') and not len(line) == 0:
                    line = fi.readline()
                        
            if var != None and table != None:
                cpts.append((var, table))
            else:
                print('Missing mandatory keys for CPT! Exiting...')
                return None, None

        if not line.startswith('VAR') and not line.startswith('CPT') and not len(line) == 0:
            line = fi.readline()


    fi.close()
    return vars, cpts

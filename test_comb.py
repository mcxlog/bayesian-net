
import itertools

##A = (['MaryCalls', 'Alarm'], [(['t', 't'], 0.7), (['t', 'f'], 0.01), (['f', 't'], 0.3), (['f', 'f'], 0.99)])
##B = (['JohnCalls', 'Alarm'], [(['t', 't'], 0.9), (['t', 'f'], 0.05), (['f', 't'], 0.1), (['f', 'f'], 0.95)])

A = (['A', 'C'], [(['t','t'], 0.2), (['t','f'],0.8),(['f', 't'], 0.4), (['f', 'f'], 0.6)])
B = (['A', 'C'], [(['t'], 0.3), (['f'], 0.7)])

##a = ['t','t','t','t','f','f','f','f']
a = ['t', 't', 't', 't','f','f', 'f', 'f']
# 4 é o total var number (repetidos)
b = list(itertools.permutations(a, 4))

newlist = []
for i in b:
  if i not in newlist:
    newlist.append(i)

print(newlist)

##c1 = ['0.7', '0.01', '0.3', '0.99']
##c2 = ['0.9', '0.05', '0.1', '0.95']
c1 = ['0.2', '0.8', '0.4', '0.6']
c2 = ['0.3', '0.7', '0.4', '0.6']

prodlist = list(itertools.product(c1, c2))


prod = [float(a)*float(b) for a,b in prodlist]

print(prod)

dependenciesA = set(A[0])
dependenciesB = set(B[0])

dependenciesOut = dependenciesA | dependenciesB

common = list(dependenciesA & dependenciesB)

table = []
indices = []
newtable = []
for item in common:
    i = 0

    if table == []:
        commonIndexA = A[0].index(item)
        commonIndexB = B[0].index(item)
      
        for tup in newlist:
            if tup[commonIndexA] == tup[len(A[0])+commonIndexB]:
                listOut = list(tup)
                del listOut[commonIndexA]
                table.append((listOut, prod[i]))
            i+=1

        depOut = A[0]+B[0]
        del depOut[commonIndexA]

        newtable = list(table)

    else:
        table = []
        table = list(newtable)
        newtable = []

        commonIndices = [l for l, x in enumerate(depOut) if x == item]
        for tup in table:
            values = tup[0]

            if values[commonIndices[0]] == values[commonIndices[1]]:
                listOut = list(values)
                del listOut[commonIndices[0]]
                newtable.append((listOut, prod[i]))
            i+=1

        del depOut[commonIndices[0]]
        
newcpt = (depOut, newtable)

        

print(newcpt)

    

